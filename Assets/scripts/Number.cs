﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Number {

    private static List<int> array;

    public static void setNumber(string number)
    {
        Number.array = Number.stringToIntArray(number);
    }

    public static void setNumber(List<int> number)
    {
        Number.array = number;
    }

    public static List<int> getNumber()
    {
        return Number.array;
    }

    public static void addNumber(int number)
    {
        List<int> addArray = Number.stringToIntArray(number.ToString());
        List<int> thisArray = Number.getNumber();
        int offset = 0;
        int retenue = 0;
        for(int i = thisArray.Count-1; i >= 0; i--)
        {
            int addPosition = addArray.Count - 1 - offset;
            if(addPosition>=0)
            {
                int add = addArray[addPosition];
                int dat = thisArray[i];
                if(add + dat + retenue <= 9)
                {
                    thisArray[i] = add + dat + retenue;
                    retenue = 0;
                }
                else
                {
                    int toAdd = add + dat + retenue;
                    List<int> toAddArray = Number.stringToIntArray(toAdd.ToString());
                    thisArray[i] = toAddArray[1];
                    retenue = toAddArray[0];
                }
            }
            offset++;
        }
        if(retenue>0)
        {
            List<int> toAddArray = Number.stringToIntArray(retenue.ToString());
            for(int k = toAddArray.Count-1; k >= 0; k--)
            {
                thisArray.Insert(0, toAddArray[k]);
            }
        }

        Number.setNumber(thisArray);
        
    }

    public static void removeNumber(int number)
    {
        List<int> removeArray = Number.stringToIntArray(number.ToString());
        List<int> thisArray = Number.getNumber();
        int offset = 0;
        int retenue = 0;
        for (int i = thisArray.Count - 1; i >= 0; i--)
        {
            int removePosition = removeArray.Count - 1 - offset;
            if (removePosition >= 0)
            {
                int remove = removeArray[removePosition];
                int dat = thisArray[i];

                if(dat < ( remove + retenue ))
                {
                    int all = dat + remove + retenue;
                    if(all > 10)
                    {
                        List<int> retenueAll = Number.stringToIntArray(all.ToString());
                        thisArray[i] = retenueAll[retenueAll.Count-1];
                        string str = "";
                        for (int v = 0; v < retenueAll.Count - 1; v++)
                        {
                            str += retenueAll[i].ToString();
                        }
                        retenue = int.Parse(str);
                    }
                    else
                    {
                        
                        thisArray[i] = (10 + dat) - (remove + retenue);
                        retenue++;
                    }

                }
                else
                {
                    thisArray[i] = dat - (remove + retenue);
                    retenue = 0;
                }
            }else
            {
                while(retenue>0)
                {
                    int dat = thisArray[i];
                    retenue -= thisArray[i];
                    thisArray[i] = 0;
                    i++;
                }
                break;
            }
            offset++;
        }
        for (int i = 0; i < thisArray.Count; i++)
        {
            if(thisArray[i] == 0)
            {
                thisArray.Remove(i);
            }
            else
            {
                break;
            }
        }
        Number.setNumber(thisArray);
    }

    private static List<int> stringToIntArray(string s)
    {
        List<int> list = new List<int>();
        foreach(char c in s.ToCharArray())
        {
            int v = (int)Char.GetNumericValue(c);
            list.Add(v);
        }
        return list;
    }

    public static int getIntNumber()
    {
        string preResult = "";
        List<int> number = Number.getNumber();
        for (int i = 0; i < number.Count; i++)
        {
            preResult += number[i].ToString();
        }
        return int.Parse(preResult);
    }

    public static string getStringNumber()
    {
        string result = "";
        List<int> number = Number.getNumber();
        for (int i = 0; i < number.Count; i++)
        {
            result += number[i].ToString();
        }
        return result;
    }

    public static string parse()
    {
        string[] numbers = new string[] { "", "K", "M", "B", "T", "q", "Q", "s", "S", "O", "N", "d", "U", "D", "!", "@", "#", "$", "%", "^", "&", "*" };
        int count = Number.getNumber().Count;
        List<int> number = Number.getNumber();

        float div = count / 4;
        string[] part = div.ToString().Split('.');
        int letter = int.Parse(part[0]);
        string prim = "";
        



        return prim + numbers[letter];
    }
}
