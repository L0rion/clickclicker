﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class menuScript : MonoBehaviour {
	public InputField inputPseudo;
	public Text error;

	// Use this for initialization
	void Start () {
		error.enabled = false;
		PlayerPrefs.DeleteKey("player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void loadGame(){
		Debug.Log (inputPseudo.text);
		if (inputPseudo.text.Length < 3) {
			error.enabled = true;
		} 
		else {
			error.enabled = false;
			PlayerPrefs.SetString("player", inputPseudo.text);
			PlayerPrefs.Save();
			Application.LoadLevel("game");
		}
	}
}
