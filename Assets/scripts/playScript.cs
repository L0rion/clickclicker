﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class playScript : MonoBehaviour
{

    public Text userPseudo;
    private Text dpsText;
    private Text moneyText;
    private static int kills = 0;
    private static double money = 0;
    private static int dps;
    public static int defaultDamage = 5;


    public void Start()
    {
        Number.setNumber(playScript.money.ToString());
        playScript.dps = playScript.defaultDamage;
        userPseudo.text = PlayerPrefs.GetString("player");
        GameObject textDPSGO = GameObject.FindWithTag("dpsActuel");
        if (textDPSGO)
        {
            dpsText = textDPSGO.GetComponent<Text>();
            dpsText.text = playScript.getDamages().ToString() + " DPS";
        }

        GameObject textMoneyGO = GameObject.FindWithTag("moneyActuel");
        if (textMoneyGO)
        {
            moneyText = textMoneyGO.GetComponent<Text>();
            moneyText.text = playScript.getMoney().ToString();
        }
    }

    public static int getDamages()
    {
        return playScript.dps;
    }

    public static int addPercentDamages(int percent)
    {
        Debug.Log(percent);
        playScript.dps = (int)System.Math.Round((float)playScript.dps * (float)(1 + (float)percent / 100));
        playScript.printDPS();
        return playScript.dps;
    }

    public static int addDamages(int dps)
    {
        playScript.dps += dps;
        playScript.printDPS();
        return playScript.dps;
    }

    public static int getKills()
    {
        return playScript.kills;
    }

    public static int addKill()
    {
        playScript.kills++;
        return getKills();
    }

    public static double getMoney()
    {
        return Number.getIntNumber();
    }

    public static double addMoney(double add)
    {
        Number.addNumber((int)add);
        playScript.printMoney();
        return Number.getIntNumber();
    }

    public static double removeMoney(double add)
    {
        Number.removeNumber((int)add);
        playScript.printMoney();
        return Number.getIntNumber();
    }

    private static void printMoney()
    {
        GameObject textGO = GameObject.FindWithTag("moneyActuel");
        if (textGO)
        {
            Text text = textGO.GetComponent<Text>();
            text.text = Number.parse();
        }
    }

    private static void printDPS()
    {
        GameObject textGO = GameObject.FindWithTag("dpsActuel");
        if (textGO)
        {
            Text text = textGO.GetComponent<Text>();
            text.text = playScript.getDamages().ToString() + " DPS";
        }
    }

    public static string parseGold(double gold)
    {
        string result = "";
        string[] numbers = new string[] {"","K","M","B","T","q","Q","s","S","O","N","d","U","D","!","@","#","$","%","^","&","*" };
        int numberId = 0;
        double prim = gold;
        double sec = 0;
        //Debug.Log(gold);
        for(int i=0; i<numbers.Length; i++)
        {
            float temp = (float)prim / 100;
            string s = temp.ToString();
            //Debug.Log(s);
            string[] parts = s.Split('.');
            if(parts[0] == "0")
            {
                numberId = i;
                break;
            }
            else
            {
                prim = Convert.ToDouble(parts[0]);
                sec = (double)Convert.ToInt64(parts[1][0] + parts[1][1]);
            }
        }
        result = prim.ToString() + numbers[numberId] + sec.ToString();
        return result;

    }


}