﻿using UnityEngine;
using System;
//using System.Collections;
using UnityEngine.UI;

public class monsterScript : MonoBehaviour
{
    public int hp;
    public int percentMonster = 20;
    public int percentBoss = 100;
    private int lifeMonster;
    private int moneyMonster;
    private int platform = 1;
    private int niveau = 1;
    public int secBossTimer;
    private bool enableTimer = false;
    private DateTime timeEnableBoss;
    public Sprite[] monsters;
    public Sprite[] boss;
    private bool failBoss = false;
    public GameObject btnBoss;
    public Text hpText;
    private bool isGround = true;
    public int moneyLvlOne = 13;
    private int moneyMin = 0;
    private int moneyMax = 0;
    public int percentMoneyAmelio = 20;
    public int percentMinMaxMoney = 2;

    // Use this for initialization
    void Start()
    {

        this.setHp(hp);
        this.setShowPlatform();
        this.setShowNiveau();
        if (this.moneyMin == 0 && this.moneyMax == 0)
        {
            this.moneyMin = (int)System.Math.Round((double)this.moneyLvlOne * (float)(1 - (float)this.percentMinMaxMoney / 100));
            this.moneyMax = (int)System.Math.Round((double)this.moneyLvlOne * (float)(1 + (float)this.percentMinMaxMoney / 100));
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (this.enableTimer)
        {
            DateTime now = DateTime.Now;

            TimeSpan span = this.timeEnableBoss - now;
            GameObject nbrEnemiesPlatforms = GameObject.Find("nbrEnemiePlateforme");
            Text text = nbrEnemiesPlatforms.GetComponent<Text>();
            if (span.TotalMilliseconds <= 0)
            {
                reMonster();
                this.btnBoss.SetActive(true);
            }
            else
            {
                text.text = span.Seconds + ":" + span.Milliseconds.ToString();
            }

        }
    }

    public void OnClickMonster()
    {
        if (this.isGround)
        {
            GameObject temp = GameObject.FindWithTag("monster");
            if (temp != null)
            {
                Image image = temp.GetComponent<Image>();
                if (image != null)
                {
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, 100);
                    this.isGround = false;
                }
            }
        }
        setHp(-playScript.getDamages());
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        this.isGround = true;
    }

    public void OnClickGoBoss()
    {
        this.btnBoss.SetActive(false);
        this.failBoss = false;
        this.newMonster((int)System.Math.Round((double)this.lifeMonster * (float)(1 + (float)this.percentBoss / 100)), ((this.moneyMin + this.moneyMax) / 2) * 2);
    }
    public void reMonster()
    {

        this.failBoss = true;
        this.niveau++;
        this.enableTimer = false;
        newMonster(this.lifeMonster, (int)(this.moneyMin + this.moneyMax) / 2);
    }


    public void newMonster(int hp, int money)
    {
        if (!this.failBoss)
        {
            if (niveau < 11)
            {
                niveau++;
                this.setHp(hp);
                this.moneyMin = (int)System.Math.Round((double)money * (float)(1 - (float)this.percentMinMaxMoney / 100));
                this.moneyMax = (int)System.Math.Round((double)money * (float)(1 + (float)this.percentMinMaxMoney / 100));

            }
            else
            {
                platform++;
                niveau = 1;
                this.enableTimer = false;
                this.setShowPlatform();
                this.setHp(this.lifeMonster);
                this.moneyMin = (int)System.Math.Round((double)moneyMonster * (float)(1 - (float)this.percentMinMaxMoney / 100));
                this.moneyMax = (int)System.Math.Round((double)moneyMonster * (float)(1 + (float)this.percentMinMaxMoney / 100));
            }
        }
        else
        {
            this.niveau = 10;
            this.enableTimer = false;
            this.setShowPlatform();
            this.setHp(this.lifeMonster);
            this.moneyMin = (int)System.Math.Round((double)moneyMonster * (float)(1 - (float)this.percentMinMaxMoney / 100));
            this.moneyMax = (int)System.Math.Round((double)moneyMonster * (float)(1 + (float)this.percentMinMaxMoney / 100));
        }


        this.setShowNiveau();


        GameObject temp = GameObject.FindWithTag("monster");
        if (temp != null)
        {
            Image image = temp.GetComponent<Image>();
            if (image != null)
            {
                if (niveau < 11)
                    image.sprite = this.monsters[UnityEngine.Random.Range(0, this.monsters.Length)];
                else
                    image.sprite = this.boss[UnityEngine.Random.Range(0, this.boss.Length)];
            }
        }
    }

    private void setHp(int current)
    {
        GameObject temp = GameObject.FindWithTag("pvMonster");
        if (temp != null)
        {
            Slider volumeSlider = temp.GetComponent<Slider>();
            if (volumeSlider != null)
            {
                volumeSlider.minValue = 0;
                if (current < 0)
                {
                    volumeSlider.value = volumeSlider.value + current;
                }
                else
                {
                    hp = current;
                    volumeSlider.maxValue = current;
                    volumeSlider.value = current;
                }
                hpText.text = volumeSlider.value.ToString();
                if (volumeSlider.value <= 0)
                {
                    int hereVal = playScript.defaultDamage;
                    for (int p = 0; p < this.platform; p++)
                    {
                        for (int n = 0; n < this.niveau; n++)
                        {
                            hereVal = (int)System.Math.Round(hereVal * (1 + (float)this.percentMonster / 100));
                        }
                    }
                    playScript.addKill();
                    playScript.addMoney(UnityEngine.Random.Range(this.moneyMin, this.moneyMax));
                    int moneyMedium = (this.moneyMin + this.moneyMax) / 2;
                    if (niveau == 10)
                    {
                        this.lifeMonster = (int)volumeSlider.maxValue;
                        this.moneyMonster = moneyMedium;
                        newMonster((int)System.Math.Round(hereVal * (1 + (float)this.percentBoss / 100)), moneyMedium * 2);
                    }
                    else
                    {
                        newMonster((int)System.Math.Round(hereVal * (float)(1 + (float)this.percentMonster / 100)), (int)System.Math.Round(moneyMedium * (float)(1 + (float)this.percentMoneyAmelio / 100)));
                    }

                }
                else if (niveau == 12)
                {
                    this.niveau = 10;
                    newMonster(current, (moneyMin + moneyMax) / 2);
                }

            }
            else
            {
                Debug.LogError("[" + temp.name + "] - Does not contain a Slider Component!");
            }

        }
        else
        {
            Debug.LogError("Slider not found");
        }
    }

    private void setShowPlatform()
    {
        GameObject platformText = GameObject.Find("Plateforme");
        Text textPlatform = platformText.GetComponent<Text>();
        textPlatform.text = "Monde " + platform;
    }

    private void setShowNiveau()
    {
        if (niveau < 11)
        {
            GameObject nbrEnemiesPlatforms = GameObject.Find("nbrEnemiePlateforme");
            Text text = nbrEnemiesPlatforms.GetComponent<Text>();
            text.text = niveau + "/10";
        }
        else
        {
            this.enableTimer = true;
            this.timeEnableBoss = DateTime.Now.AddSeconds(this.secBossTimer);
        }

    }

}
