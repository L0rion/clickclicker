﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class panelScript : MonoBehaviour
{
    public GameObject panelAlex;
    public GameObject panelEquipe;
    public GameObject panelEquipement;
    public GameObject panelBoutique;
    public GameObject panelParams;

    private bool paramsActive;

    // Use this for initialization
    void Start()
    {
        panelAlex.SetActive(true);
        panelEquipe.SetActive(false);
        panelEquipement.SetActive(false);
        panelBoutique.SetActive(false);
        panelParams.SetActive(false);
        paramsActive = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void clickPanelAlex()
    {
        panelAlex.SetActive(true);
        panelEquipe.SetActive(false);
        panelEquipement.SetActive(false);
        panelBoutique.SetActive(false);
    }
    public void clickPanelEquipe()
    {
        panelAlex.SetActive(false);
        panelEquipe.SetActive(true);
        panelEquipement.SetActive(false);
        panelBoutique.SetActive(false);
    }
    public void clickPanelEquipement()
    {
        panelAlex.SetActive(false);
        panelEquipe.SetActive(false);
        panelEquipement.SetActive(true);
        panelBoutique.SetActive(false);
    }
    public void clickPanelBoutique()
    {
        panelAlex.SetActive(false);
        panelEquipe.SetActive(false);
        panelEquipement.SetActive(false);
        panelBoutique.SetActive(true);
    }
    public void clickParam()
    {
        if (paramsActive)
        {
            panelParams.SetActive(false);
            paramsActive = false;
        }
        else
        {
            panelParams.SetActive(true);
            paramsActive = true;
        }
    }
}
