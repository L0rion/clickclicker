﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AmeliorationScript : MonoBehaviour
{


    public double price;
    private int niveau = 0;
    public int percentAmelioration = 20;
    public Text lvl;
    public Text priceText;

    // Use this for initialization
    void Start()
    {
        lvl.text = niveau.ToString();
        this.priceText.text = this.price.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void onClick()
    {
        if (playScript.getMoney() >= this.price)
        {

            this.niveau++;
            lvl.text = niveau.ToString();
            playScript.addPercentDamages(this.percentAmelioration);
            playScript.removeMoney(this.price);
            this.price = (int)System.Math.Round(this.price * (float)(1 + (float)this.percentAmelioration / 100));
            this.priceText.text = this.price.ToString();
        }
    }
}
